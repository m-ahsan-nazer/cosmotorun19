/*
    Enough C++ to get you started with Gevolution.
*/
#include <iostream>
using namespace std;

/****************************Example 1 starts ***************************************/
template <class T>
T GetMax (T a, T b) {
    T result;
    result = (a>b) ? a : b;
    return (result);
}

void eg1(){
    /*
     * Template functions
     See: http://www.cplusplus.com/doc/oldtutorial/templates/
     */
    int i=5, j=6, k;
    double l=10., m=5., n;
    k=GetMax<int>(i, j);
    n=GetMax<double>(l, m);
    cout << k << " of type " << typeid(k).name() << endl;
    cout << n << " of type " << typeid(n).name() << endl;
}

/****************************Example 2 starts ***************************************/
template <class T>
class mypair {
        T a, b;
        public:
        mypair (T first, T second)
    {
            a=first;
            b=second;
    };
        T getmax();
    };

template  <class T>
T mypair<T>::getmax(){
    T retval;
    retval = a>b? a:b;
    return retval;
}

void eg2(){
    /*
     * template classes
     */
    mypair <int> myobject (4, 75);
    cout << myobject.getmax() << endl;
    return ;
}

/****************************Example 3 starts ***************************************/
class complx{
    double x, y; //z=x+iy
    public:
        complx(double x=0., double y=0.);
        complx operator+(const complx&) const;
        void print(){
            cout << x << "+" << y << "i" << endl;
        }
};

complx::complx(double a, double b){
    x=a;
    y=b;
}

complx complx::operator+(const complx & c) const{
    complx result;
    result.x = (this->x + c.x);
    result.y = (this->y + c.y);
    return result;
}

void eg3(){
    /*
    Over loading operators.
    See: https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.1.0/com.ibm.zos.v2r1.cbclx01/cplr318.htm
    */
    complx z1(3, 4); //z1=3+4i
    complx z2(1, 1); //z2=1+i
    complx z = z1+z2;
    z.print();
}



/**************************** Main function starts ***************************************/
int main () {
    /****** Example 0 starts***********************/
    //int a, A;
    //a=1;
    //A=2;
    //cout << "a=" << a << " A=" << A<<endl;
    /****** Example 0 ends ***********************/

    //eg1();

    //eg2();
    
    //eg3();

    return 0;
}
