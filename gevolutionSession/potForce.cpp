/* 
Differentiate phi=1/r to obtain force=1/r^2 (without units) 
the LATfield2 way i.e finite difference on the lattice. 
Purpose: Gain familiarity with Site, Lattice, Field classes in LATfield2
 */
#include <iostream>
#include "LATfield2.hpp"

using namespace LATfield2;


int main(int argc, char **argv)
{
    int n,m;
    int BoxSize = 50; //not used in this example. Explicitly set on line 43
    int halo = 1;
    int dim = 3;
    //double sigma2=1.0;
    double res =1.;


    for (int i=1 ; i < argc ; i++ ){
		if ( argv[i][0] != '-' )
			continue;
		switch(argv[i][1]) {
			case 'n':
				n = atoi(argv[++i]);
				break;
			case 'm':
				m =  atoi(argv[++i]);
				break;
            //case 'b':
            //    BoxSize = atoi(argv[++i]);
            //    break;
		}
	}

	parallel.initialize(n,m);

    //Lattice lat(dim,BoxSize,halo);
    int latSize[dim] = {1,1,100};
    Lattice lat(dim, latSize, halo);

    Site x(lat);

    Field<Real> phi(lat, 1);
	Field<Real> force(lat, 3);
	Field<Real> coords(lat, 3);

	//COUT << "Lat.size " << lat.size(0)<< " " << lat.size(1) << " " << lat.size(2) << endl;
    
    for(x.first();x.test();x.next())
    {
        double r = pow(0.5 + x.coord(0) - lat.size(0)/2, 2);
        r += pow(0.5 + x.coord(1) - lat.size(1)/2, 2);
        r += pow(0.5 + x.coord(2) - lat.size(2)/2, 2);
        r = sqrt(r);
        phi(x)= 1./r;
        //phi(x)=100.0;
        //COUT << phi(x) << endl;
    }
    //phi.updateHalo();

    

    for(x.first();x.test();x.next())
    {
        force(x, 0) =  (phi(x+0) - phi(x-0))/res;
        force(x, 1) = (phi(x+1) - phi(x-1))/res;
        force(x, 2) = (phi(x+2) - phi(x-2))/res;
        
        coords(x,0) = 0.5+x.coord(0) -lat.size(0)/2;
        coords(x, 1) = 0.5+x.coord(1) - lat.size(1)/2;
        coords(x, 2) = 0.5+x.coord(2) - lat.size(2)/2;
    }
    
	phi.save("./phi.txt");
	force.save("./force.txt");
	coords.save("./coords.txt");

}
